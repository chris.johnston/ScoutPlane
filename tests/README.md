# Gitlab Runner setup
1. Create new 18.04 VM using cloud init:
#cloud-config
packages:
 - gitlab-runner
 - python3-paramiko
 - python3-pylxd
 - pycodestyle
 - flake8

runcmd:
   - [mkdir, /home/gitlab-runner/]
   - [mkdir, /home/gitlab-runner/102/]
   - [lxd, init, --auto]
   - [chown, gitlab-runner, /home/gitlab-runner/ -R]
   - [chgrp, gitlab-runner, /home/gitlab-runner/ -R]
   - [usermod, -a, -G, lxd, gitlab-runner]
   - [wget, -P, /home/gitlab-runner/102/, "https://people.canonical.com/~bryanquigley/sos/sosreport-exercise1.tar.xz", "https://people.canonical.com/~bryanquigley/sos/sosreport-exercise2.tar.xz"]
   - [wget, -P, /home/gitlab-runner/, "https://people.canonical.com/~bryanquigley/sos/sosreport-ACustomer.10101-20180904170335.tar.xz"]

2. sudo gitlab-runner register --non-interactive --url "https://gitlab.com/" --executor "shell" --run-untagged="true" --locked="true" --registration-token=(Your token from GItlab CI Runner setup)
3. Manually make a config file like /home/gitlab-runner/.config/scoutplane.ini.  We only do local testing today, so eventually this will have to be real.
[REMOTE]
server = nope
username = nope

[USER]
lp_id = nope

# TODO: 

 * Create a clear-all option for ./sp that we can use to clear all lxc containers starting with sp-*.  We can then use that in the CI build too.
 * Make an LXC container or another machine that we can actually do full SFTP testing to 

