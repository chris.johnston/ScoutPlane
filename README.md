# Scout Plane
Scout Plane makes it trivial to get sosreports and other files ready for analysis in a consistent 
and containerized manner. It is designed to work in concert with a RemoteSFTP system that houses 
said files.

Please open Issues on how this could be changed to better fit your workflow. 
Also, if you'd like to discuss or work on one of the TODOs please open an issue 
for it. 

If you are working on it, please make the pull requests do one thing at a time 
so they are easier to merge and less likely to break other work.  At the moment 
the code is rough - but evolving as we get a better idea of what's needed.

## Requirements
 * Python3
 * Snapped lxd 3.0 installed and working
 * python3-paramiko python3-pylxd
 * Ubuntu 18.04 or above
 * ScoutPlane installed from Git
 
## Setup
Prior to runnng Scout Plane for the first time, you should:
 * Setup your public key on the remote server (for some services - BrickFTP you have to add it in the web interface)
 * Connect to the remote server via SSH

The first time you run Scout Plane it will:
 * Ask you for the username and remote server location if not already configured.
 * Create an image which has the needed packages for creating new containers.

## General Usage
0. Have something that unlocks your SSH key, ssh-add, gnomey things - if you get "No authentication methods available" this is likely related.
1. Get notified by that a new file is on RemoteSSH server at $filepath
2. Run ./sp $filepath
3. Login to container and find file (and if sosreport extracted and automatically analyzed via xsos - other tools TODO)

# ScoutPlane (sp.py)
    ./sp [target-sosreport-path-on-remote-system]
  1. Launch new LXD container
  2. Fetch target sosreport to container from RemoteSSH system
  3. Launch sosreport analysis routine in container
  4. Log build & process output (IP)

## TODO
  * check before downloading a file again, inform user
  * Evalaute switching from xsos to integrating loging from pysos (it's python2 :()
  * Push analysis (xsos) reports back to RemoteSTP system
  * APPARMOR tar/xsos, other processes that process untrusted data for another level of protection (evaluate switching to Ubuntu core?)
  * LXD clustering may work at the moment, needs testing
